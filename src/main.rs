use std::{
    io::{Error, Result},
    mem,
    ptr
};

const NULL: *mut () = ptr::null_mut();

macro_rules! t {
    ($cmd:expr, $err:expr) => {{
        let result = $cmd;
        if result == $err {
            return Err(Error::last_os_error());
        }
        result
    }};
    ($cmd:expr) => { t!($cmd, !0); };
}

fn main() -> Result<()> {
    match unsafe { t!(libc::fork()) } {
        0 => child(),
        pid => Parent::new(pid).run()
    }
}
fn child() -> Result<()> {
    unsafe {
        t!(libc::ptrace(libc::PTRACE_TRACEME, 0 as libc::pid_t, NULL, NULL));
        t!(libc::raise(libc::SIGSTOP));
    }
    println!("Hello, this is a test");
    println!("So the question is, does using STDERR really terminate the program?");
    println!("That would be pretty sad");
    println!("Let's try:");
    eprintln!("Please let me in?");
    println!("Well fsck.");
    Ok(())
}

struct Parent {
    pid: libc::pid_t
}
impl Parent {
    fn new(pid: libc::pid_t) -> Self {
        Self {
            pid
        }
    }
    fn run(&self) -> Result<()> {
        self.wait()?;
        loop {
            match self.step() {
                Err(ref err) if err.raw_os_error() == Some(libc::ESRCH) => {
                    println!("Uncaught child termination.");
                    break;
                },
                Ok(Some(status)) => {
                    println!("Child terminated with error code {}.", status);
                    break;
                },
                other => { other?; }
            }
        }
        Ok(())
    }
    fn step(&self) -> Result<Option<libc::c_int>> {
        // Pre-syscall
        self.next_syscall()?;
        if let Some(status) = self.wait()? {
            return Ok(Some(status));
        }

        let mut regs = self.getregs()?;
        if regs.orig_rax == 1 && regs.rdi == 2 {
            // File is STDERR
            regs.orig_rax = 60;
            regs.rdi = 123;
            self.setregs(&regs)?;
        }

        // Post-syscall
        self.next_syscall()?;
        if let Some(status) = self.wait()? {
            return Ok(Some(status));
        }
        Ok(None)
    }

    fn wait(&self) -> Result<Option<libc::c_int>> {
        unsafe {
            let mut status = 0;
            t!(libc::waitpid(self.pid, &mut status, 0));
            if libc::WIFEXITED(status) {
                Ok(Some(libc::WEXITSTATUS(status)))
            } else {
                Ok(None)
            }
        }
    }
    fn next_syscall(&self) -> Result<()> {
        unsafe {
            t!(libc::ptrace(libc::PTRACE_SYSCALL, self.pid, NULL, NULL));
            Ok(())
        }
    }
    fn getregs(&self) -> Result<libc::user_regs_struct> {
        unsafe {
            let mut regs: libc::user_regs_struct = mem::uninitialized();
            t!(libc::ptrace(libc::PTRACE_GETREGS, self.pid, NULL, &mut regs as *mut _));
            Ok(regs)
        }
    }
    fn setregs(&self, regs: &libc::user_regs_struct) -> Result<()> {
        unsafe {
            t!(libc::ptrace(libc::PTRACE_SETREGS, self.pid, NULL, regs as *const _));
            Ok(())
        }
    }
}
